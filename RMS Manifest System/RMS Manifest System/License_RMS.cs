﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class License_RMS
    {
        public string UPSA_MEM_NO { get; set; }
        public string UPSA_LIC_NO { get; set; }
        public DateTime USPA_EXP_Date { get; set; }

    }
}
