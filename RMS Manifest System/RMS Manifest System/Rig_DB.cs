﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Rig_DB:Database
    {
        //new allRigs function
        public DataTable allRigs()
        {
            string storedProcedureName = "ALL_RIGS";
            string dataTableName = "Rigs";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public void Insert_New_Rig(string Container_Type, string Container_Colors, string Main_Canopy, string Reserve_Canopy, DateTime Reserve_Repack_Date, string AAD_Type, char Tandem, char Rental, string Jumper_ID)
        {
            string storedProcedureName = "Insert_New_Rig";
            List<string> storedProcedureVariables = new List<string>() { "@CONTAINER_TYPE", "@CONTAINER_COLORS", "@MAIN_CANOPY", "@MAIN_COLORS", "@RESERVE_CANOPY", "@RESERVE_REPACK_DATE", "@AAD_TYPE", "@TANDEM", "@RENTAL", "@JUMPER_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Char, SqlDbType.VarChar };
            List<object> Values = new List<object>() { Container_Type, Container_Colors, Main_Canopy, Reserve_Canopy, Reserve_Repack_Date, AAD_Type, Tandem, Rental, Jumper_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Update_Rig(string Rig_ID, string Container_Type, string Container_Colors, string Main_Canopy, string Reserve_Canopy, DateTime Reserve_Repack_Date, string AAD_Type, char Tandem, char Rental, string Jumper_ID)
        {
            string storedProcedureName = "Update_Rig";
            List<string> storedProcedureVariables = new List<string>() { "@RIG_ID", "@CONTAINER_TYPE", "@CONTAINER_COLORS", "@MAIN_CANOPY", "@MAIN_COLORS", "@RESERVE_CANOPY", "@RESERVE_REPACK_DATE", "@AAD_TYPE", "@TANDEM", "@RENTAL", "@JUMPER_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Char, SqlDbType.VarChar };
            List<object> Values = new List<object>() { Rig_ID, Container_Type, Container_Colors, Main_Canopy, Reserve_Canopy, Reserve_Repack_Date, AAD_Type, Tandem, Rental, Jumper_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_Rig(string Rig_ID)
        {
            string storedProcedureName = "Delete_Rig";
            List<string> storedProcedureVariables = new List<string>() { "@RIG_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Rig_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public List<string> Retrieve_RigForUpdate(string Rig_ID)
        {
            string storedProcedureName = "Retrieve_Rig";
            string dataTableName = "Rig";
            List<string> storedProcedureVariables = new List<string>() { "@Rig_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Rig_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> rigInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    rigInfo.Add(item.ToString());
                }

            }

            return rigInfo;
        }

        public DataTable Retrieve_RigForDisplay(string Rig_ID)
        {
            string storedProcedureName = "Retrieve_Rig";
            string dataTableName = "Rig";
            List<string> storedProcedureVariables = new List<string>() { "@Rig_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Rig_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        //copy set up of the jumper db class
    }
}
