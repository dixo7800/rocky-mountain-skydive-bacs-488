﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Plane_DB:Database
    {
        public void Add_Plane(string plane_make, string plane_model, string fuel_type, string jumper_capacity, DateTime annual_date, DateTime service_date)
        {
            string storedProcedureName = "Insert_New_Plane";
            List<string> storedProcedureVariables = new List<string>() { "@plane_make", "@plane_model", "@fuel_type", "@jumper_capacity", "@annual_date", "@service_date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.Date };
            List<object> Values = new List<object>() { plane_make, plane_model, fuel_type, jumper_capacity, annual_date, service_date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Update_Plane(string plane_id, string plane_make, string plane_model, string fuel_type, string jumper_capacity, DateTime annual_date, DateTime service_date)
        {
            string storedProcedureName = "Update_Plane";
            List<string> storedProcedureVariables = new List<string>() { "@plane_id", "@plane_make", "@plane_model", "@fuel_type", "@jumper_capacity", "@annual_date", "@service_date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.Date };
            List<object> Values = new List<object>() { plane_id, plane_make, plane_model, fuel_type, jumper_capacity, annual_date, service_date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_Plane(string plane_id)
        {
            string storedProcedureName = "Delete_Plane";
            List<string> storedProcedureVariables = new List<string>() { "@plane_id" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { plane_id };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }


        public List<string> Retrieve_PlaneForUpdate(string plane_id)
        {
            string storedProcedureName = "Retrieve_Plane";
            string dataTableName = "Plane";
            List<string> storedProcedureVariables = new List<string>() { "@plane_id" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { plane_id };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> planeInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    planeInfo.Add(item.ToString());
                }

            }

            return planeInfo;
        }

        public DataTable Retrieve_PlaneForDisplay(string plane_id)
        {
            string storedProcedureName = "Retrieve_Plane";
            string dataTableName = "Plane";
            List<string> storedProcedureVariables = new List<string>() { "@plane_id" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { plane_id };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        //need to make stored procedure method for plane capacity dictionary return for manifest plane dropdown menu
        public Dictionary<object, object> Plane_ID_Capacity(string Plane_ID)
        {
            string storedProcedureName = "Plane_ID_Capacity";
            string dataTableName = "Plane";
            List<string> storedProcedureVariables = new List<string>() { "@Plane_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Plane_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            //Need to go through the datatable and pull the values into the dictionary where the plane id is the key and the capacity is the value!!!!!!!!!!!!!!!!!!

            //this is just a temporary solution - would rather dump directly into a dictionary but know how to make list<string>, so doing this for now
            List<string> planeInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    planeInfo.Add(item.ToString());
                }

            }

            Dictionary<object, object> dict = new Dictionary<object, object>();

            for (int i = 0; i<planeInfo.Count(); i = i+2)
            {
                dict.Add(planeInfo[i], planeInfo[i + 1]);
            }

            return dict;
        }

        public string PLANE_ID_Lookup(string PLANE_ID)
        {
            string result = "";

            string storedProcedureName = "PLANE_ID_Lookup";
            string dataTableName = "PLANE_ID";
            List<string> storedProcedureVariables = new List<string>() { "@PLANE_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { PLANE_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> plane_pk = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    plane_pk.Add(item.ToString());
                }

            }

            result = plane_pk[0];

            return result;
        }
    }
}
