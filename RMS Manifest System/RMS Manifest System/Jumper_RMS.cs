﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Jumper_RMS
    {
        public string Jumper_ID { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Street { get; set; }
        public string Apt_No { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public DateTime Date_Of_Birth { get; set; }
        public string Phone_No { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string Emergency_Contact { get; set; }
        public string Staff { get; set; }
        public string UPSA_MEM_NO { get; set; }

        public string ErrorChecking_Add()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("First Name", First_Name);
            dict.Add("Last Name", Last_Name);
            dict.Add("Street", Street);
            dict.Add("City", City);
            dict.Add("State", State);
            dict.Add("Zip", Zip);
            dict.Add("Date Of Birth", Date_Of_Birth);
            dict.Add("Phone Number", Phone_No);
            dict.Add("Weight", Weight);
            dict.Add("Height", Height);
            dict.Add("Emergency Contact", Emergency_Contact);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "")
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }

        public string ErrorChecking_Update()
        {
            string message = "";

            Dictionary<object, object> dict = new Dictionary<object, object>();
            dict.Add("Jumper ID", Jumper_ID);
            dict.Add("First Name", First_Name);
            dict.Add("Last Name", Last_Name);
            dict.Add("Street", Street);
            dict.Add("City", City);
            dict.Add("State", State);
            dict.Add("Zip", Zip);
            dict.Add("Date Of Birth", Date_Of_Birth);
            dict.Add("Phone Number", Phone_No);
            dict.Add("Weight", Weight);
            dict.Add("Height", Height);
            dict.Add("Emergency Contact", Emergency_Contact);

            foreach (var d in dict)
            {
                //make sure null is first in case the object being referenced is null otherwise it tries the toString method on a null which throws an error
                if (d.Value == null || d.Value.ToString() == "")
                {
                    message = "The " + d.Key + " field can not be left blank. Please add in the information.";
                    return message;
                }
            }

            return message;
        }

        private string ForeignKeyCheck_USPAMEMNO(string USPA_MEM_NO)
        {
            string message = "";

            
            License_DB lic = new License_DB();
            string value = lic.USPA_MEM_NO_Lookup(USPA_MEM_NO);

            if(value == ""){
                message = "";
            }
            else{
                message = " The USPA_MEMBER_NUMBER that was referenced is not in the database. Please add the USPA_MEMBER_NUMBER that was referenced before continuing. ";
            }
            
            
            return message;
        }

        public string ForeignKey_Check(string USPA_MEM_NO)
        {
            string message = "";

            string a = ForeignKeyCheck_USPAMEMNO(USPA_MEM_NO);

            message = message + a;

            return message;
        }
    }
}
