﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Rig_RMS
    {
        public string Rig_ID { get; set; }
        public string Container_Type { get; set; }
        public string Container_Colors { get; set; }
        public string Main_Canopy { get; set; }
        public string Main_Colors { get; set; }
        public string Reserver_Canopy { get; set; }
        public DateTime Reserver_Repack_Date { get; set; }
        public string AAD_Type { get; set; }
        public string Tandem { get; set; }
        public string Rental { get; set; }
        public string Jumper_ID { get; set; }

    }
}
