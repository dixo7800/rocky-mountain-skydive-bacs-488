﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Jumps.xaml
    /// </summary>
    public partial class Jumps : Window
    {
        public Jumps()
        {
            InitializeComponent();
        }

        private void btnUpdateJump_Click(object sender, RoutedEventArgs e)
        {
            if(txtLoad_UpdateJump.Text == "" || txtLoad_UpdateJump.Text == null)
            {
                MessageBox.Show("The Load field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (txtPlaneID_UpdateJump.Text == "" || txtPlaneID_UpdateJump.Text == null)
            {
                MessageBox.Show("The Plane ID field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (txtCostCode_UpdateJump.Text == "" || txtCostCode_UpdateJump.Text == null)
            {
                MessageBox.Show("The Cost Code field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            //This one needs to be tested. Can not be certain this is the correct way to check for a blank/null value. May need to do something with selecteddate.value.date - Shawn
            else if (JumpDate_UpdateJump.Text == "" || JumpDate_UpdateJump.Text == null)
            {
                MessageBox.Show("The Jump Date field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (LoadDate_UpdateJump.Text == "" || LoadDate_UpdateJump.Text == null)
            {
                MessageBox.Show("The Load Date field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
        }
    }
}
