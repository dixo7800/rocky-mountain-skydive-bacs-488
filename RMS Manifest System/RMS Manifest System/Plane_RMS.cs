﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Plane_RMS
    {
        public string Plane_ID { get; set; }
        public string Plane_Make { get; set; }
        public string Plane_Model { get; set; }
        public string Fuel_Type { get; set; }
        public string Jumper_Capacity { get; set; }
        public DateTime Annual_Date { get; set; }
        public DateTime Service_Date { get; set; }

    }
}
