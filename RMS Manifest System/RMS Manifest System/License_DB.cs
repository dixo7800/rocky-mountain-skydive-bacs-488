﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class License_DB:Database
    {
        public DataTable All_License()
        {
            string storedProcedureName = "ALL_License";
            string dataTableName = "Licenses";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);
            return dt;
        }

        public void Insert_New_License(string USPA_MEM_NO, string USPA_LIC_NO, DateTime USPA_EXP_DATE)
        {
            string storedProcedureName = "Insert_New_License";
            List<string> storedProcedureVariables = new List<string>() { "@USPA_MEM_NO", "@USPA_LIC_NO", "UPSA_EXP_DATE" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { USPA_MEM_NO, USPA_LIC_NO, USPA_EXP_DATE  };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Update_License(string USPA_MEM_NO, string USPA_LIC_NO, DateTime USPA_EXP_DATE)
        {
            string storedProcedureName = "Update_License";
            List<string> storedProcedureVariables = new List<string>() { "@USPA_MEM_NO", "@USPA_LIC_NO", "UPSA_EXP_DATE" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { USPA_MEM_NO, USPA_LIC_NO, USPA_EXP_DATE };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_License(string USPA_MEM_NO)
        {
            string storedProcedureName = "Delete_License";
            List<string> storedProcedureVariables = new List<string>() { "@USPA_MEM_NO" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { USPA_MEM_NO };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public List<string> Retrieve_LicenseForUpdate(string USPA_MEM_NO)
        {
            string storedProcedureName = "Retrieve_License";
            string dataTableName = "Licenses";
            List<string> storedProcedureVariables = new List<string>() { "@USPA_MEM_NO" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { USPA_MEM_NO };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> loadInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    loadInfo.Add(item.ToString());
                }

            }

            return loadInfo;
        }

        public DataTable Retrieve_LicenseForDisplay(string USPA_MEM_NO)
        {
            string storedProcedureName = "Retrieve_License";
            string dataTableName = "Licenses";
            List<string> storedProcedureVariables = new List<string>() { "@USPA_MEM_NO" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { USPA_MEM_NO };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public string USPA_MEM_NO_Lookup(string USPA_MEMBER_NO)
        {
            string result = "";

            string storedProcedureName = "USPA_MEMBER_NO_Lookup";
            string dataTableName = "USPA_MEM_NOs";
            List<string> storedProcedureVariables = new List<string>() { "@USPA_MEMBER_NO" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { USPA_MEMBER_NO };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> uspaMemNO = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    uspaMemNO.Add(item.ToString());
                }

            }

            result = uspaMemNO[0];

            return result;
        }
    }
}
