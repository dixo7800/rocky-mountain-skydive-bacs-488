﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for RMS_Sign_Up_Window.xaml
    /// </summary>
    public partial class RMS_Sign_Up_Window : Window
    {
        public RMS_Sign_Up_Window()
        {
            InitializeComponent();
            Loaded += RMS_Sign_Up_Window_Loaded;
        }

        private void RMS_Sign_Up_Window_Loaded(object sender, RoutedEventArgs e)
        {
            First_Name_TxtBx.Focus();
        }

        private void Cancel_Btn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            mw.Show();
            this.Close();
        }

        private void Sign_Up_Btn_Click(object sender, RoutedEventArgs e)
        {
            //error checking, all boxes have data
            if (First_Name_TxtBx.Text == null || First_Name_TxtBx.Text == "" || Last_Name_TxtBx.Text == null || Last_Name_TxtBx.Text == "" ||
                Username_TxtBx.Text == null || Username_TxtBx.Text == "" || Password_PassBx.Password == null || Password_PassBx.Password == "" ||
                Re_Entry_PassBx.Password == null || Re_Entry_PassBx.Password == "")
            {
                //error message for missing info
                MessageBox.Show("Information is missing, please make sure all fields have been filled out. Thank you.","Missing Information", MessageBoxButton.OK);
            }

            //check if passwords match
            else if (Password_PassBx.Password == Re_Entry_PassBx.Password)
            {
                //need to hash both the username and password, so they are protected in the database and set to new variables so the user does not see the hash values
                string u_name = RMS_Security.SHA512(Username_TxtBx.Text);
                string p_word = RMS_Security.SHA512(Password_PassBx.Password);

                //new database object
                //Database db = new Database();
                Admin_DB db = new Admin_DB();

                //need to check if the username has been used already or not
                //pass u_name hashed version, to username checker, if return username, dont let process continue, if return null, username isnt used yet
                List<string> ls = db.usernameExistCheck(u_name);
                //this if statement may need a different condition
                Console.WriteLine(ls);
                Console.Read();
                //Console.WriteLine(ls[0]);
                if (ls.Count != 0)
                {
                    if (ls[0] != null || ls[0] != "")
                    {
                        MessageBox.Show("The username you have entered is already in use. Please select another username.", "Username Error", MessageBoxButton.OK);
                    }
                    
                }
                else
                {
                    //put rest of code in here to allow for successful sign up
                    //this is where we will pass info into database
                    db.InsertNewAdmin(First_Name_TxtBx.Text, Last_Name_TxtBx.Text, u_name, p_word);

                    //message for success
                    MessageBox.Show("Thank you for signing up. We have collected your information and you will be added to our system.\n" +
                        "\rYou will be sent back to the home screen to sign-in.", "Success", MessageBoxButton.OK);

                    //return to main screen to login, this is a check to make sure sign-up worked correctly
                    MainWindow mw = new MainWindow();
                    mw.Show();
                    this.Close();
                }

            }
            else
            {
                //give error message for incorrect password
                MessageBox.Show("The passwords you entered do not match. Please try again.","Password Mismatch", MessageBoxButton.OK);
                Password_PassBx.Password = "";
                Re_Entry_PassBx.Password = "";
                
            }
        }
    }
}
