﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Load_RMS
    {
        public string Load_NO { get; set; }
        public DateTime Load_Date { get; set; }
        public string Pilot { get; set; }
        public string Fuel_Load { get; set; }

    }
}
