﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Costs_DB:Database
    {
        public DataTable All_Costs()
        {
            string storedProcedureName = "All_Cost";
            string dataTableName = "Costs";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public void Insert_New_Cost(string Cost_Name, string Cost_Price)
        {
            string storedProcedureName = "Insert_New_Cost";
            List<string> storedProcedureVariables = new List<string>() { "@cost_name", "@cost_price" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { Cost_Name, Cost_Price };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Update_Cost(string Cost_Code, string Cost_Name, string Cost_Price)
        {
            string storedProcedureName = "Update_Cost";
            List<string> storedProcedureVariables = new List<string>() { "@cost_code", "@cost_name", "@cost_price" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { Cost_Code, Cost_Name, Cost_Price };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_Cost(string Cost_Code, string Cost_Name, string Cost_Price)
        {
            string storedProcedureName = "Delete_Cost";
            List<string> storedProcedureVariables = new List<string>() { "@cost_code", "@cost_name", "@cost_price" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { Cost_Code, Cost_Name, Cost_Price };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public List<string> Retrieve_CostForUpdate(string Cost_Code)
        {
            string storedProcedureName = "Retrieve_Cost";
            string dataTableName = "Cost";
            List<string> storedProcedureVariables = new List<string>() { "@cost_code" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Cost_Code };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> costInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    costInfo.Add(item.ToString());
                }

            }

            return costInfo;
        }

        public DataTable Retrieve_CostForDisplay(string Cost_Code)
        {
            string storedProcedureName = "Retrieve_Cost";
            string dataTableName = "Cost";
            List<string> storedProcedureVariables = new List<string>() { "@cost_code" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Cost_Code };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public string COST_CODE_Lookup(string COST_CODE)
        {
            string result = "";

            string storedProcedureName = "COST_CODE_Lookup";
            string dataTableName = "COST_CODE";
            List<string> storedProcedureVariables = new List<string>() { "@COST_CODE" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { COST_CODE };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> costs_pk = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    costs_pk.Add(item.ToString());
                }

            }

            result = costs_pk[0];

            return result;
        }
    }
}
