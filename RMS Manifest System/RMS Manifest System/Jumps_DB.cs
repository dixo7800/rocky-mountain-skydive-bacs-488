﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Jumps_DB:Database
    {
        public DataTable All_Jumps()
        {
            string storedProcedureName = "ALL_Jump";
            string dataTableName = "Jumps";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);
            return dt;
        }

        public void Insert_New_Jumps(DateTime Jump_Date, string Style, string Jumper_ID, string Load_No, DateTime Load_Date, string Plane_ID, string Cost_Code)
        {
            string storedProcedureName = "Insert_New_Jump";
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_DATE", "@STYLE", "@JUMPER_ID", "@LOAD_NO", "@LOAD_DATE", "@PLANE_ID", "@COST_CODE" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { Jump_Date, Style, Jumper_ID, Load_No, Load_Date, Plane_ID, Cost_Code };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Update_Jumps(string Jump_ID, DateTime Jump_Date, string Style, string Jumper_ID, string Load_No, DateTime Load_Date, string Plane_ID, string Cost_Code)
        {
            string storedProcedureName = "Update_Jump";
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_ID", "@JUMP_DATE", "@STYLE", "@JUMPER_ID", "@LOAD_NO", "@LOAD_DATE", "@PLANE_ID", "@COST_CODE" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { Jump_ID, Jump_Date, Style, Jumper_ID, Load_No, Load_Date, Plane_ID, Cost_Code };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_Jumps(string Jump_ID)
        {
            string storedProcedureName = "Delete_Jump";
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Jump_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public List<string> Retrieve_JumpsForUpdate(string Jump_ID)
        {
            string storedProcedureName = "Retrieve_Jump";
            string dataTableName = "Jumps";
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Jump_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> loadInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    loadInfo.Add(item.ToString());
                }

            }

            return loadInfo;
        }

        public DataTable Retrieve_JumpsForDisplay(string Jump_ID)
        {
            string storedProcedureName = "Retrieve_Jump";
            string dataTableName = "Jumps";
            List<string> storedProcedureVariables = new List<string>() { "@JUMP_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Jump_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }
    }
}
