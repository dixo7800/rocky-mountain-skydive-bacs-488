﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Admin_DB:Database
    {
        public void InsertNewAdmin(string firstName, string lastName, string userName, string password)
        {
            string storedProcedureName = "ADD_NEW_ADMIN";
            List<string> storedProcedureVariables = new List<string>() { "@u_name", "@p_word", "@f_name", "@l_name" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { userName, password, firstName, lastName };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        //new retireve admin login function
        //need stored procedure
        public List<string> retrieveAdminLoginInfo(string username, string password)
        {
            string storedProcedureName = "ADMIN_LOGIN";
            string dataTableName = "ADMIN";

            List<string> storedProcedureVariables = new List<string>() { "@u_name", "@p_word" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { username, password };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> adminList = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    adminList.Add(item.ToString());
                }

            }

            return adminList;
        }

        //new username exist check function
        public List<string> usernameExistCheck(string u_name)
        {
            string storedProcedureName = "ADMIN_UNAME_CHECK";
            string dataTableName = "ADMIN";

            List<string> storedProcedureVariables = new List<string>() { "@u_name" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { u_name };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> unameList = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                unameList.Add(row[0].ToString());
            }

            return unameList;
        }
    }
}
