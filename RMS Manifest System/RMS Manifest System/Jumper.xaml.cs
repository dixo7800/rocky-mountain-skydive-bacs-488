﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Jumper.xaml
    /// </summary>
    public partial class Jumper : Window
    {
        public Jumper()
        {
            InitializeComponent();

            Loaded += Jumper_Loaded;

        }

        private void Jumper_Loaded(object sender, RoutedEventArgs e)
        {
            Jumper_DB db = new Jumper_DB();

            DataTable dt = db.allJumpers();
            if (dt != null)
            {
                //JumperDataGrid.ItemsSource = dt.DefaultView;
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAddJumper_Click(object sender, RoutedEventArgs e)
        {
            Jumper_RMS jumper = new Jumper_RMS();
            jumper.First_Name = txtFirstName.Text;
            jumper.Last_Name = txtLastName.Text;
            jumper.Street = txtStreet.Text;
            jumper.City = txtCity.Text;
            jumper.State = txtState.Text;
            jumper.Zip = txtZip.Text;
            jumper.Date_Of_Birth = DateOfBirth.SelectedDate.Value.Date;
            jumper.Phone_No = txtPhone.Text;
            jumper.Weight = txtWeight.Text;
            jumper.Height = txtHeight.Text;
            jumper.Emergency_Contact = txtEmergencyContact.Text + "@" + emergencyPhoneTxtBx.Text;

            jumper.Apt_No = txtAptNo.Text;
            jumper.UPSA_MEM_NO = txtUSPAMemNo.Text;

            string errorCheckingOutput = jumper.ErrorChecking_Add();

            if(errorCheckingOutput == "")
            {
                if (txtAptNo.Text == "" || txtAptNo.Text == null)
                {
                    txtAptNo.Text = null;
                    jumper.Apt_No = txtAptNo.Text;
                }

                if (txtUSPAMemNo.Text == "" || txtUSPAMemNo.Text == null)
                {
                    txtUSPAMemNo.Text = null;
                    jumper.Apt_No = txtAptNo.Text;
                }

                //set the appropriate staff attribute for the db - Shawn
                string Staff = "";
                if (staffTrueRadBtn.IsChecked == true)
                {
                    Staff = "TRUE";
                    jumper.Staff = Staff;
                }
                else
                {
                    Staff = "FALSE";
                    jumper.Staff = Staff;
                }

                

                Jumper_DB add_DB = new Jumper_DB();
                add_DB.AddJumper(jumper);
            }
            else
            {
                MessageBox.Show(errorCheckingOutput, "Missing Information", MessageBoxButton.OK);
            }

            

        }

        private void btnUpdateJumper_Click(object sender, RoutedEventArgs e)
        {
            //need way to open the message box based on the tab being selected - Shawn
            
            // SHOW THE UPDATE JUMPER TABLE
            if (txtFirstName_UpdateJumper.Text == "" || txtFirstName_UpdateJumper.Text == null)
            {
                MessageBox.Show("The First Name field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (txtLastName_UpdateJumper.Text == "" || txtLastName_UpdateJumper.Text == null)
            {
                MessageBox.Show("The Last Name field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (txtStreet_UpdateJumper.Text == "" || txtStreet_UpdateJumper.Text == null)
            {
                MessageBox.Show("The Street field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (txtCity_UpdateJumper.Text == "" || txtCity_UpdateJumper.Text == null)
            {
                MessageBox.Show("The City field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (txtState_UpdateJumper.Text == "" || txtState_UpdateJumper.Text == null)
            {
                MessageBox.Show("The State field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (txtZip_UpdateJumper.Text == "" || txtZip_UpdateJumper.Text == null)
            {
                MessageBox.Show("The Zip field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            //This one needs to be tested. Can not be certain this is the correct way to check for a blank/null value. May need to do something with selecteddate.value.date - Shawn
            else if (DateOfBirth_UpdateJumper.Text == "" || DateOfBirth_UpdateJumper.Text == null)
            {
                MessageBox.Show("The Date of Birth field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (txtPhone_UpdateJumper.Text == "" || txtPhone_UpdateJumper.Text == null)
            {
                MessageBox.Show("The Phone field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (txtWeight_UpdateJumper.Text == "" || txtWeight_UpdateJumper.Text == null)
            {
                MessageBox.Show("The Weight field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (txtheight_UpdateJumper.Text == "" || txtheight_UpdateJumper.Text == null)
            {
                MessageBox.Show("The Height field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }
            else if (txtEmergencyContact_UpdateJumper.Text == "" || txtEmergencyContact_UpdateJumper.Text == null)
            {
                MessageBox.Show("The Emergency Contact field can not be left blank. Please add in the information.", "Missing Information", MessageBoxButton.OK);
            }

            if (txtAptNoUpdateJumper.Text == "" || txtAptNoUpdateJumper.Text == null)
            {
                txtAptNoUpdateJumper.Text = null;
            }

            if (txtUSPAMemNoUpdateJumper.Text == "" || txtUSPAMemNoUpdateJumper.Text == null)
            {
                txtUSPAMemNoUpdateJumper.Text = null;
            }

            //set the appropriate staff attribute for the db - Shawn
            string Staff_UpdateJumper = "";
            if (staffTrueRadBtnUpdate.IsChecked == true)
            {
                Staff_UpdateJumper = "TRUE";
            }
            else
            {
                Staff_UpdateJumper = "FALSE";
            }

            string emergencyContactUpdate = txtEmergencyContact_UpdateJumper.Text + "@" + emePhoneTxtBxUpdateJumper.Text;

            Jumper_DB update_DB = new Jumper_DB();
            update_DB.UpdateJumper(jumperIDtxt_UpdateJumper.Content.ToString(),txtFirstName_UpdateJumper.Text,txtLastName_UpdateJumper.Text,txtStreet_UpdateJumper.Text,txtAptNoUpdateJumper.Text,txtCity_UpdateJumper.Text,txtState_UpdateJumper.Text,txtZip_UpdateJumper.Text,DateOfBirth_UpdateJumper.SelectedDate.Value.Date,txtPhone_UpdateJumper.Text,txtWeight_UpdateJumper.Text,txtheight_UpdateJumper.Text,emergencyContactUpdate,Staff_UpdateJumper,txtUSPAMemNoUpdateJumper.Text);
        }

        private void btnDeleteJumper_Click(object sender, RoutedEventArgs e)
        {
            // ADMIN PERMISSION REQUIRED TO ACCESS THIS FORM
            // SHOW DELETE JUMPER FORM ONCE ADMIN CREDENTIALS ACCEPTED

            if (jumper_id_txtbx_delete.Text == "" || jumper_id_txtbx_delete.Text == null)
            {
                MessageBox.Show("A Jumper_ID must be entered in order for this to function correctly.", "Missing Jumper_ID", MessageBoxButton.OK);
            }
            else
            {
                Jumper_DB deleteJumperDB = new Jumper_DB();
                deleteJumperDB.DeleteJumper(jumper_id_txtbx_delete.Text);
            }
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // DO WE NEED THIS HERE?  NOT SURE WHAT THIS FUNCTION SHOULD DO
        }

        private void searchJumperBtn_Click(object sender, RoutedEventArgs e)
        {
            if(jumper_id_txtbx_search.Text == "" || jumper_id_txtbx_search.Text == null)
            {
                MessageBox.Show("A Jumper_ID must be entered in order for this to function correctly.", "Missing Jumper_ID", MessageBoxButton.OK);
            }
            else
            {
                Jumper_DB searchJumperDB = new Jumper_DB();
                DataTable dt = searchJumperDB.retrieveJumperForDisplay(jumper_id_txtbx_search.Text);
                dataGridJumper.ItemsSource = dt.DefaultView;
            }
        }

        private void allJumpersBtn_Click(object sender, RoutedEventArgs e)
        {
            Jumper_DB allJumpers = new Jumper_DB();
            DataTable dt = allJumpers.allJumpers();
            dataGridJumper.ItemsSource = dt.DefaultView;
        }

        private void customersBtn_Click(object sender, RoutedEventArgs e)
        {
            Jumper_DB allCustomers = new Jumper_DB();
            DataTable dt = allCustomers.allCustomers();
            dataGridJumper.ItemsSource = dt.DefaultView;
        }

        private void allEmployeesBtn_Click(object sender, RoutedEventArgs e)
        {
            Jumper_DB allEmployees = new Jumper_DB();
            DataTable dt = allEmployees.allEmployees();
            dataGridJumper.ItemsSource = dt.DefaultView;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

            switch (tabItem)
            {
                //case "Item1":
                    //break;

                case "Update Jumper":
                    //input box is the name of the code at end of grid
                    //UpdateInputBox.Visibility = Visibility.Visible;
                    Update_Jumper_ID_DialogWindow ujid = new Update_Jumper_ID_DialogWindow();
                    ujid.ShowDialog();
                    string jumper_ID_For_Update = "";
                    jumper_ID_For_Update = ujid.jumper_ID;
                    ujid.Close();

                    if (jumper_ID_For_Update == "" || jumper_ID_For_Update == null)
                    {
                        break;
                    }
                    else
                    {
                        Jumper_DB ret_db = new Jumper_DB();
                        List<string> retrievedJumperInfo = ret_db.retrieveJumperForUpdate(jumper_ID_For_Update);
                        jumperIDtxt_UpdateJumper.Content = retrievedJumperInfo[0];
                        txtFirstName_UpdateJumper.Text = retrievedJumperInfo[1];
                        txtLastName_UpdateJumper.Text = retrievedJumperInfo[2];
                        txtStreet_UpdateJumper.Text = retrievedJumperInfo[3];
                        txtAptNoUpdateJumper.Text = retrievedJumperInfo[4];
                        txtCity_UpdateJumper.Text = retrievedJumperInfo[5];
                        txtState_UpdateJumper.Text = retrievedJumperInfo[6];
                        txtZip_UpdateJumper.Text = retrievedJumperInfo[7];
                        DateOfBirth_UpdateJumper.Text = retrievedJumperInfo[8];
                        txtPhone_UpdateJumper.Text = retrievedJumperInfo[9];
                        txtWeight_UpdateJumper.Text = retrievedJumperInfo[10];
                        txtheight_UpdateJumper.Text = retrievedJumperInfo[11];
                        //txtEmergencyContact_UpdateJumper.Text = retrievedJumperInfo[12];
                        //txtemerphone_UpdateJumper.Text = retrievedJumperInfo[13];
                        string fullEmeContact = retrievedJumperInfo[12];
                        List<string> abc = fullEmeContact.Split('@').ToList();
                        txtEmergencyContact_UpdateJumper.Text = abc[0];
                        emePhoneTxtBxUpdateJumper.Text = abc[1];
                        string staff = retrievedJumperInfo[13];
                        if (staff == "TRUE" || staff == "True")
                        {
                            staffTrueRadBtnUpdate.IsChecked = true;
                            staffFalseRadBtnUpdate.IsChecked = false;
                        }
                        else
                        {
                            staffFalseRadBtnUpdate.IsChecked = true;
                            staffTrueRadBtnUpdate.IsChecked = false;
                        }
                        txtUSPAMemNoUpdateJumper.Text = retrievedJumperInfo[14];
                    }

                break;

                default:
                    return;
            }
        }

        //added random comments to fix push issue
        //more comments to fix stuff.
    }
}
