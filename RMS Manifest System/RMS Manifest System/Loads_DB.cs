﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Loads_DB:Database
    {
        public DataTable All_Loads()
        {
            string storedProcedureName = "ALL_Loads";
            string dataTableName = "Loads";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);
            return dt;
        }

        public void Insert_New_Rig(DateTime Load_Date, string Pilot, string Fuel_Load)
        {
            string storedProcedureName = "Insert_New_Loads";
            List<string> storedProcedureVariables = new List<string>() { "@load_date", "@pilot", "@fuel_load" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { Load_Date, Pilot, Fuel_Load };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Update_Rig(string Load_No, DateTime Load_Date, string Pilot, string Fuel_Load)
        {
            string storedProcedureName = "Update_Loads";
            List<string> storedProcedureVariables = new List<string>() { "@load_no", "@load_date", "@pilot", "@fuel_load" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date, SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { Load_No, Load_Date, Pilot, Fuel_Load};

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_Load(string Load_No, DateTime Load_Date)
        {
            string storedProcedureName = "Delete_Loads";
            List<string> storedProcedureVariables = new List<string>() { "@load_no", "@load_date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date };
            List<object> Values = new List<object>() { Load_No, Load_Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public List<string> Retrieve_LoadForUpdate(string Load_No, DateTime Load_Date)
        {
            string storedProcedureName = "Retrieve_Loads";
            string dataTableName = "Loads";
            List<string> storedProcedureVariables = new List<string>() { "@load_no", "@load_date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date };
            List<object> Values = new List<object>() { Load_No, Load_Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> loadInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    loadInfo.Add(item.ToString());
                }

            }

            return loadInfo;
        }

        public DataTable Retrieve_LoadForDisplay(string Load_No, DateTime Load_Date)
        {
            string storedProcedureName = "Retrieve_Loads";
            string dataTableName = "Loads";
            List<string> storedProcedureVariables = new List<string>() { "@load_no", "@load_date" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date };
            List<object> Values = new List<object>() { Load_No, Load_Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public string LOAD_NO_LOAD_DATE_Lookup(string Load_No, DateTime Load_Date)
        {
            string result = "";

            string storedProcedureName = "LOAD_NO_LOAD_DATE_Lookup";
            string dataTableName = "Loads PK Info";
            List<string> storedProcedureVariables = new List<string>() { "@LOAD_NO", "@LOAD_DATE" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.Date };
            List<object> Values = new List<object>() { Load_No, Load_Date };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> loads_pk = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    loads_pk.Add(item.ToString());
                }

            }

            result = loads_pk[0];

            return result;
        }
    }
}
