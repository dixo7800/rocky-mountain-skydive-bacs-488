﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RMS_Manifest_System
{
    /// <summary>
    /// Interaction logic for Data_View_Window.xaml
    /// </summary>
    public partial class Data_View_Window : Window
    {
        public Data_View_Window()
        {
            InitializeComponent();
        }

        private void All_Jumpers_Btn_Click(object sender, RoutedEventArgs e)
        {
            //Database db = new Database();
            Jumper_DB db = new Jumper_DB();

            DataTable dt = db.allJumpers();
            if (dt != null)
            {
                dataGrid1.ItemsSource = dt.DefaultView;
            }
            
        }
    }
}
