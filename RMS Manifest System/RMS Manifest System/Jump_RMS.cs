﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Jump_RMS
    {
        public string Jump_ID { get; set; }
        public DateTime Jump_Date { get; set; }
        public string Style { get; set; }
        public string Jumper_ID { get; set; }
        public string Load_NO { get; set; }
        public DateTime Load_Date { get; set; }
        public string Plane_ID { get; set; }
        public string Cost_Code { get; set; }

    }
}
