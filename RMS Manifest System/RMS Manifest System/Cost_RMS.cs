﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Cost_RMS
    {
        public string Cost_Code { get; set; }
        public string Cost_Name { get; set; }
        public string Cost_Price { get; set; }

    }
}
