﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Database
    {
        //public string ConString = "Data Source=.\\SQLEXPRESS;Initial Catalog=RMS1;Integrated Security=True";
        public string ConString = "Data Source=rms-sql-server.database.windows.net;Initial Catalog=RMS_Database;User Id=rms_unco;Password=Rocky-Mountain-Skydive";
        //public string ConString = "Server = tcp:rms-sql-server.database.windows.net,1433;Initial Catalog = RMS_Database; Persist Security Info=False;User ID =rms_unco; Password=Rocky-Mountain-Skydive; MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout = 30";
        public string CmdString = string.Empty;

        public void ExecuteQueryNoReturn(string storedProcedureName, SqlCommand cmdPassed)
        {
            //idk why it needs two connection strings, its very weird!
            try
            {

                using (SqlConnection con = new SqlConnection(ConString))
                {
                    SqlConnection conn = new SqlConnection(ConString);

                    SqlCommand cmd = cmdPassed;
                    cmd.CommandText = storedProcedureName;
                    cmd.Connection = conn;

                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();


                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e + " this is the error.");

            }
        }

        public DataTable ExecuteQueryDataTable(string storedProcedureName, SqlCommand cmdPassed, string dataTableName)
        {
            try
            {

                using (SqlConnection con = new SqlConnection(ConString))
                {
                    SqlConnection conn = new SqlConnection(ConString);

                    SqlCommand cmd = cmdPassed;
                    cmd.CommandText = storedProcedureName;
                    cmd.Connection = conn;

                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();

                    DataTable dt = new DataTable(dataTableName);
                    sda.Fill(dt);

                    return dt;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e + " this is the error.");

                return null;
            }

        }

        public SqlCommand CMD_Builder(List<string> storedProcedureVariables, List<SqlDbType> SQL_DataTypes,
            List<object> Values)
        {
            SqlCommand cmd = new SqlCommand();

            for (int i = 0; i < storedProcedureVariables.Count; i++)
            {
                string parameterName = storedProcedureVariables[i];
                SqlDbType dataType = SQL_DataTypes[i];
                cmd.Parameters.Add(parameterName, dataType);
                cmd.Parameters[parameterName].Value = Values[i];
            }

            return cmd;
        }
    }
}
