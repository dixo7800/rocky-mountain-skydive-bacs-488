﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_Manifest_System
{
    class Ratings_DB:Database
    {
        public DataTable All_Ratings()
        {
            string storedProcedureName = "ALL_Rating";
            string dataTableName = "Ratings";

            SqlCommand cmd = new SqlCommand();

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);
            return dt;
        }

        public void Insert_New_Ratings(string Rating)
        {
            string storedProcedureName = "Insert_New_Rating";
            List<string> storedProcedureVariables = new List<string>() { "@Rating" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Rating };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Update_Ratings(string Rating_ID, string Rating)
        {
            string storedProcedureName = "Update_Rating";
            List<string> storedProcedureVariables = new List<string>() { "@Rating_ID", "Rating" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar, SqlDbType.VarChar };
            List<object> Values = new List<object>() { Rating_ID, Rating };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public void Delete_Ratings(string Rating_ID)
        {
            string storedProcedureName = "Delete_Rating";
            List<string> storedProcedureVariables = new List<string>() { "@Rating_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Rating_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            ExecuteQueryNoReturn(storedProcedureName, cmd);
        }

        public List<string> Retrieve_RatingsForUpdate(string Rating_ID)
        {
            string storedProcedureName = "Retrieve_Rating";
            string dataTableName = "Ratings";
            List<string> storedProcedureVariables = new List<string>() { "@Rating_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Rating_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> loadInfo = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    loadInfo.Add(item.ToString());
                }

            }

            return loadInfo;
        }

        public DataTable Retrieve_RatingsForDisplay(string Rating_ID)
        {
            string storedProcedureName = "Retrieve_Rating";
            string dataTableName = "Ratings";
            List<string> storedProcedureVariables = new List<string>() { "@Rating_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { Rating_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            return dt;
        }

        public string RATING_ID_Lookup(string RATING_ID)
        {
            string result = "";

            string storedProcedureName = "RATING_ID_Lookup";
            string dataTableName = "RATING_ID";
            List<string> storedProcedureVariables = new List<string>() { "@RATING_ID" };
            List<SqlDbType> SQLDataTypes = new List<SqlDbType>() { SqlDbType.VarChar };
            List<object> Values = new List<object>() { RATING_ID };

            SqlCommand cmd = CMD_Builder(storedProcedureVariables, SQLDataTypes, Values);

            DataTable dt = ExecuteQueryDataTable(storedProcedureName, cmd, dataTableName);

            List<string> ratings_pk = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    ratings_pk.Add(item.ToString());
                }

            }

            result = ratings_pk[0];

            return result;
        }
    }
}
